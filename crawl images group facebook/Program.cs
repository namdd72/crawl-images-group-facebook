﻿using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace crawl_images_group_facebook
{
    class Program
    {
        private static string userid;
        private static string ajaxpipe_token;
        private static string async_get_token;
        private static string spin_r;
        private static string spin_b;
        private static string spin_t;

        private static string group;
        private static string cookie;

        static void Main(string[] args)
        {
            //group = "gaixinhchonloc";
            //group = "khoacongnghethongtintlu";
            var cookiePath = "./Cookie.txt";

            Console.Write("Group: ");
            group = Console.ReadLine();
            if (File.Exists(cookiePath)) cookie = File.ReadAllText(cookiePath);
            else
            {
                Console.Write("Cookie: ");
                cookie = Console.ReadLine();
                File.WriteAllText(cookiePath, cookie);
            }

            if (!Directory.Exists(group)) Directory.CreateDirectory(group);

            var url = $"https://www.facebook.com/groups/{group}/photos";
            var html = getHTML(url);

            var pattern = "USER_ID\":\"(?<userid>.*?)\"";
            userid = Regex.Match(html, pattern, RegexOptions.Singleline).Groups["userid"].Value;

            pattern = "ajaxpipe_token\":\"(?<ajaxpipe_token>.*?)\"|async_get_token\":\"(?<async_get_token>.*?)\"";
            var regex = Regex.Matches(html, pattern, RegexOptions.Singleline);
            (ajaxpipe_token, async_get_token) = (regex[0].Groups["ajaxpipe_token"].Value, regex[1].Groups["async_get_token"].Value);

            pattern = "spin_r\":(?<spin_r>.*?),|spin_b\":\"(?<spin_b>.*?)\"|spin_t\":(?<spin_t>.*?),";
            regex = Regex.Matches(html, pattern, RegexOptions.Singleline);
            (spin_r, spin_b, spin_t) = (regex[0].Groups["spin_r"].Value, regex[1].Groups["spin_b"].Value, regex[2].Groups["spin_t"].Value);

        download:;
            pattern = "aria-label=\"Hình ảnh\"(.*?)href=\"(?<href>.*?)\"(.*?)(background-image: url\\((?<links>.*?)\\)|data-src=\"(?<data_src>.*?)\")";
            regex = Regex.Matches(html, pattern, RegexOptions.Singleline);

            var href_links = regex.Select(i => (i.Groups["href"].Value, (!string.IsNullOrEmpty(i.Groups["links"].Value) ? i.Groups["links"].Value : i.Groups["data_src"].Value))).ToList();

            Download(href_links);

            pattern = "GroupPhotosetPagelet\",(?<GroupPhotosetPagelet>.*?),{";
            var groupPhotosetPagelet = Regex.Match(html, pattern, RegexOptions.Singleline).Groups["GroupPhotosetPagelet"].Value;

        check: if (string.IsNullOrEmpty(groupPhotosetPagelet) || !(groupPhotosetPagelet.StartsWith("{") && groupPhotosetPagelet.EndsWith("}")))
            {
                if (pattern == "{\"scroll_load\"(.*?)}") return;

                pattern = "{\"scroll_load\"(.*?)}";
                groupPhotosetPagelet = Regex.Match(html, pattern, RegexOptions.Singleline).Value;

                goto check;
            }

            url = $"https://www.facebook.com/ajax/pagelet/generic.php/GroupPhotosetPagelet?fb_dtsg_ag={async_get_token}&ajaxpipe=1&ajaxpipe_token={ajaxpipe_token}&no_script_path=1&data={WebUtility.UrlEncode(groupPhotosetPagelet)}&__user={userid}&__a=1&__csr=&__req=fetchstream_1&__beoa=0&__pc=PHASED:DEFAULT&dpr=1&__s=nvzl6z:zb6vnm:tueia0&__comet_req=0&jazoest=27866&__spin_r={spin_r}&__spin_b={spin_b}&__spin_t={spin_t}&__adt=1&ajaxpipe_fetch_stream=1";
            html = getHTML(url);

            goto download;
        }

        private static void Download(List<(string, string)> href_links)
        {
            if (href_links.Count > 15)
            {
                var task1 = new Task(() => Download(href_links.GetRange(0, href_links.Count / 2)));
                var task2 = new Task(() => Download(href_links.GetRange(href_links.Count / 2, href_links.Count - href_links.Count / 2)));

                task1.Start(); task2.Start();
                Task.WaitAll(task1, task2);

                return;
            }

            var webClient = new WebClient();
            string fbid = "", set = "";

            foreach (var i in href_links)
            {
                var (href, link) = i;

                if (link.IndexOf("static.xx.fbcdn.net") != -1) continue;

                var tmp = Regex.Match(link, @"/\d+(.*?)\?").Value;
                var filename = tmp.Substring(1, tmp.Length - 2);
                var path = $"./{group}/{filename}";

                if (File.Exists(path))
                {
                    Console.WriteLine($"Skip {filename}");

                    continue;
                }
                Console.WriteLine($"Download {filename}");

                try
                {
                    var regex = Regex.Matches(href, "fbid=(?<fbid>.*?)&|set=(?<set>.*?)&", RegexOptions.Singleline);
                    (fbid, set) = (regex[0].Groups["fbid"].Value, regex[1].Groups["set"].Value);
                }
                catch
                {
                    var regex = Regex.Matches(href, @"g\.\d+|\d+", RegexOptions.Singleline);
                    (fbid, set) = (regex[1].Value, regex[0].Value);
                }

            retry: try
                {
                    var data = "{\"type\":\"1\",\"ifg\":\"1\",\"fbid\":\"" + fbid + "\",\"set\":\"" + set + "\",\"external_log_id\":null,\"player_origin\":null,\"firstLoad\":true}";
                    var url = $"https://www.facebook.com/ajax/pagelet/generic.php/PhotoViewerInitPagelet?fb_dtsg_ag={WebUtility.UrlEncode(async_get_token)}&ajaxpipe=1&ajaxpipe_token={ajaxpipe_token}&no_script_path=1&data={WebUtility.UrlEncode(data)}&__user={userid}&__a=1&__csr=&__beoa=0&__pc=PHASED%3ADEFAULT&dpr=1&__comet_req=0&__spin_r={spin_r}&__spin_b={spin_b}&__spin_t={spin_t}&ajaxpipe_fetch_stream=1";

                    var html = getHTML(url);

                    var linkimg = Regex.Match(html, "image\":{\"(.*?)\"url\":\"(?<linkimg>.*?)\"", RegexOptions.Singleline).Groups["linkimg"].Value;

                    webClient.DownloadFile(linkimg, path);
                }
                catch
                {
                    Console.WriteLine($"Download {filename} > Fail");

                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    Console.WriteLine($"Download {filename} > Retry");

                    goto retry;
                }
            }
        }

        private static string getHTML(string url)
        {
            var client = new RestClient(url);
            client.Timeout = -1;

            var request = new RestRequest(Method.GET);
            request.AddHeader("cookie", cookie);
            IRestResponse response = client.Execute(request);

            return WebUtility.HtmlDecode(response.Content).Replace("\\", "");
        }
    }
}